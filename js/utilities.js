// All of these functions are in the global scope

"use strict";

// returns mouse position in local coordinate system of element
function getMouse(e){
	var mouse = {} // make an object
	mouse.x = e.pageX - e.target.offsetLeft;
	mouse.y = e.pageY - e.target.offsetTop;
	return mouse;
}

function getRandom(min, max) {
	return Math.random() * (max - min) + min;
}

function nextInt(i) {
	return Math.floor(Math.random() * i);
}

function makeColor(red, green, blue, alpha){
	var color='rgba('+red+','+green+','+blue+', '+alpha+')';
	return color;
}

// Function Name: getRandomColor()
// returns a random color of alpha 1.0
// http://paulirish.com/2009/random-hex-color-code-snippets/
function getRandomColor(){
	var red = Math.round(Math.random()*200+55);
	var green = Math.round(Math.random()*200+55);
	var blue=Math.round(Math.random()*200+55);
	var color='rgb('+red+','+green+','+blue+')';
	// OR	if you want to change alpha
	// var color='rgba('+red+','+green+','+blue+',0.50)'; // 0.50
	return color;
}

function getRandomUnitVector(){
	var x = getRandom(-1,1);
	var y = getRandom(-1,1);
	var length = Math.sqrt(x*x + y*y);
	if(length == 0){ // very unlikely
		x=1; // point right
		y=0;
		length = 1;
	} else{
		x /= length;
		y /= length;
	}

	return {x:x, y:y};
}

function simplePreload(imageArray){
	// loads images all at once
	for (var i = 0; i < imageArray.length; i++) {
		var img = new Image();
		img.src = imageArray[i];
	}
}


function loadImagesWithCallback(sources, callback) {
	var imageObjects = [];
	var numImages = sources.length;
	var numLoadedImages = 0;

	for (var i = 0; i < numImages; i++) {
		imageObjects[i] = new Image();
		imageObjects[i].onload = function() {
		numLoadedImages++;
		// console.log("loaded image at '" + this.src + "'")
		if(numLoadedImages >= numImages) {
			callback(imageObjects); // send the images back
		}
		};
		
		imageObjects[i].src = sources[i];
	}
	}

// Determines whether a block should be composed into the chunk mesh
// @param { number } b - the block to test
// @param { 1 = transparent } type - What type is it
function isBlockSolid(b, type)
{
	return worldGen.TYPE_CUBE[b] && (type === 1 ? worldGen.TYPE_OPAQUE[b] : !(worldGen.TYPE_OPAQUE[b]));
}

// Optimization trick - _only do this once_, since it never changes.
var standardMeshData = (function() {
	// Vertex tex-coords - map into a mega-texture
	let meshVt = [];
	// Vertex normals - there are really only six of these (because cube)
	let meshVn = [];

	// This will fill out the mega-texture, which allows for 32 different textures
	for (let i = 0; i < 32; ++i)
	{
		// x and y are offset values
		let x = (i % 4) / 4.0;
		let y = Math.floor(i / 4) / 8.0;

		let s = 0.25;

		meshVt.push(("vt " + (0.00 * s + x) + " " + (0.50 * s + y) + " " + "\n"));
		meshVt.push(("vt " + (0.25 * s + x) + " " + (0.50 * s + y) + " " + "\n"));
		meshVt.push(("vt " + (0.50 * s + x) + " " + (0.50 * s + y) + " " + "\n"));

		meshVt.push(("vt " + (0.00 * s + x) + " " + (0.25 * s + y) + " " + "\n"));
		meshVt.push(("vt " + (0.25 * s + x) + " " + (0.25 * s + y) + " " + "\n"));
		meshVt.push(("vt " + (0.50 * s + x) + " " + (0.25 * s + y) + " " + "\n"));
		meshVt.push(("vt " + (0.75 * s + x) + " " + (0.25 * s + y) + " " + "\n"));
		meshVt.push(("vt " + (1.00 * s + x) + " " + (0.25 * s + y) + " " + "\n"));

		meshVt.push(("vt " + (0.00 * s + x) + " " + (0.00 * s + y) + " " + "\n"));
		meshVt.push(("vt " + (0.25 * s + x) + " " + (0.00 * s + y) + " " + "\n"));
		meshVt.push(("vt " + (0.50 * s + x) + " " + (0.00 * s + y) + " " + "\n"));
		meshVt.push(("vt " + (0.75 * s + x) + " " + (0.00 * s + y) + " " + "\n"));
		meshVt.push(("vt " + (1.00 * s + x) + " " + (0.00 * s + y) + " " + "\n"));
	}

	meshVn.push(("vn " + 0 + " " + 0 + " " + 1 + "\n"));
	meshVn.push(("vn " + 0 + " " + 0 + " " + -1 + "\n"));
	meshVn.push(("vn " + 0 + " " + 1 + " " + 0 + "\n"));
	meshVn.push(("vn " + 0 + " " + -1 + " " + 0 + "\n"));
	meshVn.push(("vn " + 1 + " " + 0 + " " + 0 + "\n"));
	meshVn.push(("vn " + -1 + " " + 0 + " " + 0 + "\n"));

	let meshVtS = meshVt.reduce(function(a, b) { return a + b; }, "");
	let meshVnS = meshVn.reduce(function(a, b) { return a + b; }, "");

	return meshVtS + meshVnS;
})();

// Generates a string that the rendering engine can convert into a mesh on the GPU
// Route the return of this straight into initMesh in graphics
// @param { array? } ochunk - you should probably send it a chunk
// @param { 1 = transparent } type - what chunk type are we building
function generateChunkMesh(chunk, type)
{
	// Vertex positions - the only one that actually needs to be built
	let meshV = [];
	// Faces - this one gets a little more interesting I suppose
	let meshF = [];

	// Number of faces created thus far
	let faces = 0;

	// I can't believe I'm reverse-engineering my own rendering engine
	for (let i = 0; i < worldGen.CHUNK_HEIGHT *
						worldGen.CHUNK_SIZE *
						worldGen.CHUNK_SIZE; ++i)
	{
		let x = i % worldGen.CHUNK_SIZE;
		let y = Math.floor(i / (worldGen.CHUNK_SIZE * worldGen.CHUNK_SIZE));
		let z = Math.floor(i / worldGen.CHUNK_SIZE) % worldGen.CHUNK_SIZE;

		let block = chunk.get(x, y, z);

		// Don't bother with this block if it isn't real - abort
		if (!(isBlockSolid(block, type))) { continue; }

		// At this point, we are certain the block exists - check each side and build

		var bX = (-0.50 + x), aX = (0.50 + x),
			bY = (-0.50 + y), aY = (0.50 + y),
			bZ = (-0.50 + z), aZ = (0.50 + z);

		let f = faces * 4 + 1;
		let b = (block - 1) * 13 + 1;

		// Left - side note : I love short-circuit evaluation
		if (x == 0 || !(isBlockSolid(chunk.get(x - 1, y, z), type)))
		{
			meshV.push(("v " + bX + " " + bY + " " + aZ + "\n"));
			meshV.push(("v " + bX + " " + aY + " " + bZ + "\n"));
			meshV.push(("v " + bX + " " + bY + " " + bZ + "\n"));
			meshV.push(("v " + bX + " " + aY + " " + aZ + "\n"));

			meshF.push("f " + (f+0) + "/" + (b+9) + "/" + (6) +
						" " + (f+1) + "/" + (b+3) + "/" + (6) +
						" " + (f+2) + "/" + (b+8) + "/" + (6) + "\n" );

			meshF.push("f " + (f+0) + "/" + (b+9) + "/" + (6) +
						" " + (f+3) + "/" + (b+4) + "/" + (6) +
						" " + (f+1) + "/" + (b+3) + "/" + (6) + "\n" );

			faces += 1;
			f = faces * 4 + 1;
		}

		// Right
		if (x == worldGen.CHUNK_SIZE - 1 || !(isBlockSolid(chunk.get(x + 1, y, z), type)))
		{
			meshV.push(("v " + aX + " " + bY + " " + bZ + "\n"));
			meshV.push(("v " + aX + " " + aY + " " + aZ + "\n"));
			meshV.push(("v " + aX + " " + bY + " " + aZ + "\n"));
			meshV.push(("v " + aX + " " + aY + " " + bZ + "\n"));

			meshF.push("f " + (f+0) + "/" + (b+11) + "/" + (5) +
						" " + (f+1) + "/" + (b+5) + "/" + (5) +
						" " + (f+2) + "/" + (b+10) + "/" + (5) + "\n" );

			meshF.push("f " + (f+0) + "/" + (b+11) + "/" + (5) +
						" " + (f+3) + "/" + (b+6) + "/" + (5) +
						" " + (f+1) + "/" + (b+5) + "/" + (5) + "\n" );

			faces += 1;
			f = faces * 4 + 1;
		}

		// Front
		if (z == worldGen.CHUNK_SIZE - 1 || !(isBlockSolid(chunk.get(x, y, z + 1), type)))
		{
			meshV.push(("v " + aX + " " + bY + " " + aZ + "\n"));
			meshV.push(("v " + bX + " " + aY + " " + aZ + "\n"));
			meshV.push(("v " + bX + " " + bY + " " + aZ + "\n"));
			meshV.push(("v " + aX + " " + aY + " " + aZ + "\n"));

			meshF.push("f " + (f+0) + "/" + (b+10) + "/" + (1) +
						" " + (f+1) + "/" + (b+4) + "/" + (1) +
						" " + (f+2) + "/" + (b+9) + "/" + (1) + "\n" );

			meshF.push("f " + (f+0) + "/" + (b+10) + "/" + (1) +
						" " + (f+3) + "/" + (b+5) + "/" + (1) +
						" " + (f+1) + "/" + (b+4) + "/" + (1) + "\n" );

			faces += 1;
			f = faces * 4 + 1;
		}

		// Back
		if (z == 0 || !(isBlockSolid(chunk.get(x, y, z - 1), type)))
		{
			meshV.push(("v " + bX + " " + bY + " " + bZ + "\n"));
			meshV.push(("v " + aX + " " + aY + " " + bZ + "\n"));
			meshV.push(("v " + aX + " " + bY + " " + bZ + "\n"));
			meshV.push(("v " + bX + " " + aY + " " + bZ + "\n"));

			meshF.push("f " + (f+0) + "/" + (b+12) + "/" + (2) +
						" " + (f+1) + "/" + (b+6) + "/" + (2) +
						" " + (f+2) + "/" + (b+11) + "/" + (2) + "\n" );

			meshF.push("f " + (f+0) + "/" + (b+12) + "/" + (2) +
						" " + (f+3) + "/" + (b+7) + "/" + (2) +
						" " + (f+1) + "/" + (b+6) + "/" + (2) + "\n" );

			faces += 1;
			f = faces * 4 + 1;
		}

		// Top
		if (y == worldGen.CHUNK_HEIGHT - 1 || !(isBlockSolid(chunk.get(x, y + 1, z), type)))
		{
			meshV.push(("v " + aX + " " + aY + " " + aZ + "\n"));
			meshV.push(("v " + bX + " " + aY + " " + bZ + "\n"));
			meshV.push(("v " + bX + " " + aY + " " + aZ + "\n"));
			meshV.push(("v " + aX + " " + aY + " " + bZ + "\n"));

			meshF.push("f " + (f+0) + "/" + (b+5) + "/" + (3) +
						" " + (f+1) + "/" + (b+1) + "/" + (3) +
						" " + (f+2) + "/" + (b+4) + "/" + (3) + "\n" );

			meshF.push("f " + (f+0) + "/" + (b+5) + "/" + (3) +
						" " + (f+3) + "/" + (b+2) + "/" + (3) +
						" " + (f+1) + "/" + (b+1) + "/" + (3) + "\n" );

			faces += 1;
			f = faces * 4 + 1;
		}

		// Bottom
		if (y == 0 || !(isBlockSolid(chunk.get(x, y - 1, z), type)))
		{
			meshV.push(("v " + bX + " " + bY + " " + aZ + "\n"));
			meshV.push(("v " + aX + " " + bY + " " + bZ + "\n"));
			meshV.push(("v " + aX + " " + bY + " " + aZ + "\n"));
			meshV.push(("v " + bX + " " + bY + " " + bZ + "\n"));

			meshF.push("f " + (f+0) + "/" + (b+3) + "/" + (4) +
						" " + (f+1) + "/" + (b+1) + "/" + (4) +
						" " + (f+2) + "/" + (b+4) + "/" + (4) + "\n" );

			meshF.push("f " + (f+0) + "/" + (b+3) + "/" + (4) +
						" " + (f+3) + "/" + (b+0) + "/" + (4) +
						" " + (f+1) + "/" + (b+1) + "/" + (4) + "\n" );

			faces += 1;
			f = faces * 4 + 1;
		}
	}

	let meshVS = meshV.reduce(function(a, b) { return a + b; }, "");
	let meshFS = meshF.reduce(function(a, b) { return a + b; }, "");

	return meshVS + standardMeshData + meshFS;
}

/*
Function Name: clamp(val, min, max)
Author: Web - various sources
Return Value: the constrained value
Description: returns a value that is
constrained between min and max (inclusive) 
*/
function clamp(val, min, max){
	return Math.max(min, Math.min(max, val));
}


 // FULL SCREEN MODE
function requestFullscreen(element) {
	if (element.requestFullscreen) {
		element.requestFullscreen();
	} else if (element.mozRequestFullscreen) {
		element.mozRequestFullscreen();
	} else if (element.mozRequestFullScreen) { // camel-cased 'S' was changed to 's' in spec
		element.mozRequestFullScreen();
	} else if (element.webkitRequestFullscreen) {
		element.webkitRequestFullscreen();
	}
	// .. and do nothing if the method is not supported
};


// This gives Array a randomElement() method
Array.prototype.randomElement = function(){
	return this[Math.floor(Math.random() * this.length)];
}