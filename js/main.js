/*jslint browser:true */
/*jslint plusplus: true */

/* imported app */
var app = app || {};

app.main = app.main || (
	function () {
		"use strict";
		return {
			// Properties
			GAME: ({
				WIDTH : window.innerWidth - 20,
				HEIGHT: window.innerHeight - 20,
			}),
			GAME_STATE: Object.freeze({
				LOADING: 0,
				BEGIN: 1,
				DEFAULT: 2,
				PAUSED: 3
			}),
			canvas: undefined,
			
			// Used by calculateDeltaTime() 
			lastTime: 0,
			debug: true,
			handleKeyPress: false,
			animationID: 0,
			gameState: undefined,
			
			// Keyboard input handling
			myKeys: undefined,
			
			// Will provide a rendering API to go through WebGL
			graphics : undefined,
			standardTextures: { diffuseTexture : "assets/textures/master.png", specularTexture : "assets/textures/masterSpecular.png", emissionTexture : "assets/textures/masterEmission.png" },

			// Audio stuff
			audio : undefined,
			musicPaused : false,
			musicPlayer : null,

			genWorker: undefined,
			genMessage: "",
			genStr: "          ",
			genPercent: 0.0.toFixed(2),
			
			pmouse : null,
			
			sun : null,
			// Time it takes to cycle through a day
			cycleTime: 240000,
			worldTime: 0,
			
			musicPlayer : null,
			
			torchParticle : null,
			torch : null,
			torchLight : null,
			
			sunRender : null,
			moonRender : null,
			
			// methods
			init : function() {
				// Initialize properties
				this.canvas = document.querySelector("#txCanvas");
				this.canvas.onmousedown = this.doMousedown.bind(this);
				document.addEventListener('pointerlockchange', this.lockChangeAlert.bind(this), false);
				document.addEventListener('mozpointerlockchange', this.lockChangeAlert.bind(this), false);

				this.graphics.init(this.GAME.WIDTH, this.GAME.HEIGHT);
				// this.graphics.loadMesh("assets/meshes/stairs.obj");
				
				this.audio.loadSound("assets/sounds/BossaBossa.mp3", "bossa");
				
				this.musicPlayer = new AudioPlayer("bossa");
				this.musicPlayer.time = 3.0;
				
				this.graphics.loadTexture("assets/textures/master.png");
				this.graphics.loadTexture("assets/textures/masterSpecular.png");
				this.graphics.loadTexture("assets/textures/masterEmission.png");
				
				this.graphics.loadTexture("assets/textures/torchDiffuse.png");
				this.graphics.loadTexture("assets/textures/torchEmission.png");
				
				this.graphics.loadTexture("assets/textures/sun.png");
				this.graphics.loadTexture("assets/textures/moon.png");
				
				this.sun = new DirectionalLight({ direction : $V([-1,-1,-1]).toUnitVector(), intensity : $V([0.4, 0.36, 0.32]) });
				this.sun.register();
				
				this.sunRender = new MeshRenderable({
					mesh : "assets/meshes/cube.obj",
					rotation : $V([0.0, -Math.PI / 2, 0.0]),
					scale : $V([10, 10, 10]),
					textures : {
						emissionTexture : "assets/textures/sun.png"
					}
				});
				this.sunRender.register();
				
				this.moonRender = new MeshRenderable({
					mesh : "assets/meshes/cube.obj",
					rotation : $V([0.0, -Math.PI / 2, 0.0]),
					scale : $V([10, 10, 10]),
					textures : {
						emissionTexture : "assets/textures/moon.png"
					}
				});
				this.moonRender.register();

				
				window.onresize = (function(e)
				{
					this.GAME.WIDTH = window.innerWidth - 20;
					this.GAME.HEIGHT = window.innerHeight - 20;
					this.graphics.resize(this.GAME.WIDTH, this.GAME.HEIGHT);
				}.bind(this));

				this.gameState = this.GAME_STATE.LOADING;

				if(window.Worker) {
					this.genWorker = new Worker("js/generation.js");
					this.genWorker.onmessage = function(e) {
						var data = e.data;
						this.genMessage = data.genMessage;
						this.genStr = data.genStr;
						this.genPercent = data.genPercent;
						if(data.meshData) {
							var meshData = data.meshData;
							for(var i = 0; i < meshData.str.length; i++) {
								var tex = "chunk" + meshData.chunkIndex + "-" + i;
								this.graphics.initMesh(meshData.str[i], tex);
								var mesh = new MeshRenderable({
									textures : this.standardTextures,
									mesh : tex,
									posOnly : true,
									opaque : i != 0,
									position : $V([meshData.chunkX, 0, meshData.chunkZ]),
								});
								mesh.register();
							}
						}
						if(data.finished) {
							this.graphics.getActiveCamera().transform.position = Vector.create([-10, 60, 50]);
							this.graphics.getActiveCamera().transform.rotation = $V([-0.6, 0, 0]);
							this.gameState = this.GAME_STATE.BEGIN;
						}
					}.bind(this);
					// Start
					this.genWorker.postMessage([]);
				}
				else {
					this.genMessage = "Browser too old to support this game!";
				}
				
				this.torchParticle = new ParticleRenderable({});
				this.torchParticle.register();
				
				this.torch = new MeshRenderable({ scale : $V([0.05, 0.4, 0.05]), rotation : $V([-0.2, 0.0, 0.2]), textures : { diffuseTexture : "assets/textures/torchDiffuse.png", emissionTexture : "assets/textures/torchEmission.png" } });
				this.torch.register();
				
				this.torchLight = new PointLight({ intensity : $V([0.6, 0.5, 0.3]), radius : 20.0 });
				this.torchLight.register();

				// Start the game loop
				this.update();
			},
			
			update: function() {
				this.animationID = requestAnimationFrame(this.update.bind(this));

				var dt = this.calculateDeltaTime();
				var cam = this.graphics.getActiveCamera().transform;

				if(this.gameState === this.GAME_STATE.LOADING) {
					this.graphics.clear();
					this.graphics.drawText("WEBCRAFT", this.GAME.WIDTH / 2 - 128, this.GAME.HEIGHT / 2 - 50, "32pt \"Ubuntu Mono\"", "#22ff22");
					this.graphics.drawText("By Ashwin Ganapathiraju and Kenneth Holland", this.GAME.WIDTH / 2 - 186, this.GAME.HEIGHT / 2 - 20, "10pt \"Ubuntu Mono\"", "#ff2222");
					this.graphics.drawText(this.genMessage, this.GAME.WIDTH / 2 - (this.genMessage.length * 8), this.GAME.HEIGHT / 2 + 20, "16pt \"Ubuntu Mono\"", "#fff");
					this.graphics.drawText("[" + this.genStr + "] " + this.genPercent + "%", this.GAME.WIDTH / 2 - 162, this.GAME.HEIGHT / 2 + 80, "18pt \"Ubuntu Mono\"", "#fff");
				}
				else if(this.gameState === this.GAME_STATE.BEGIN) {
					this.graphics.clear();
					this.graphics.drawText("WEBCRAFT", this.GAME.WIDTH / 2 - 128, this.GAME.HEIGHT / 2 - 50, "32pt \"Ubuntu Mono\"", "#22ff22");
					this.graphics.drawText("By Ashwin Ganapathiraju and Kenneth Holland", this.GAME.WIDTH / 2 - 186, this.GAME.HEIGHT / 2 - 20, "10pt \"Ubuntu Mono\"", "#ff2222");
					this.graphics.drawText("Click to start", this.GAME.WIDTH / 2 - 112, this.GAME.HEIGHT / 2 + 20, "16pt \"Ubuntu Mono\"", "#fff");
					this.graphics.drawText("Instructions:", this.GAME.WIDTH / 2 - 117, this.GAME.HEIGHT / 2 + 80, "18pt \"Ubuntu Mono\"", "#fff");
					this.graphics.drawText("WASDEQ to move", this.GAME.WIDTH / 2 - 126, this.GAME.HEIGHT / 2 + 115, "18pt \"Ubuntu Mono\"", "A0A0A0");
					this.graphics.drawText("Arrows to pan", this.GAME.WIDTH / 2 - 117, this.GAME.HEIGHT / 2 + 140, "18pt \"Ubuntu Mono\"", "A0A0A0");
					this.graphics.drawText("PO for music", this.GAME.WIDTH / 2 - 108, this.GAME.HEIGHT / 2 + 165, "18pt \"Ubuntu Mono\"", "A0A0A0");
					this.worldTime = this.cycleTime / 4;
				}
				else if(this.gameState === this.GAME_STATE.PAUSED) {
					this.graphics.clear();
					this.graphics.drawText("PAUSED", this.GAME.WIDTH / 2 - 54, this.GAME.HEIGHT / 2 - 9, "18pt \"Ubuntu Mono\"", "#fff");
				}
				else if(this.gameState === this.GAME_STATE.DEFAULT)
				{
					if(this.handleKeyPress)
					{
						this.keyCheck(dt);
					}

					this.graphics.draw(dt);

					this.musicPlayer.time -= dt;
					if (this.musicPlayer.time <= 0.0)
					{
						this.musicPlayer.time = 170.0;
						this.musicPlayer.play();
					}
				}

				if (this.debug) {
					var pos = cam.position.elements;
					// Draw camera in top left corner
					this.graphics.drawText("x : " + (pos[0]).toFixed(1), 8, 20, "10pt \"Ubuntu Mono\"", "#A0A0A0");
					this.graphics.drawText("y : " + (pos[1]).toFixed(1), 8, 32, "10pt \"Ubuntu Mono\"", "#A0A0A0");
					this.graphics.drawText("z : " + (pos[2]).toFixed(1), 8, 44, "10pt \"Ubuntu Mono\"", "#A0A0A0");
					// Draw rtime in top right corner
					this.graphics.drawText(this.readableTime(), this.GAME.WIDTH - 60, 20, "10pt \"Ubuntu Mono\"", "#A0A0A0");
					// Draw fps in bottom left corner
					this.graphics.drawText("fps : " + (1/dt).toFixed(3), 8, this.GAME.HEIGHT - 10, "18pt \"Ubuntu Mono\"", "#A0A0A0");
					// Draw dt in bottom right corner
					this.graphics.drawText("dt : " + dt.toFixed(3), this.GAME.WIDTH - 150, this.GAME.HEIGHT - 10, "18pt \"Ubuntu Mono\"", "#A0A0A0");
				}
				this.handleSky();
			},

			keyCheck: function(dt) {
				var cam = this.graphics.getActiveCamera().transform;
				let yaw = cam.rotation.elements[1];
				if (this.myKeys.keydown[87]) // forward - w
				{
					cam.position.elements[0] -= Math.sin(yaw) * 20 * dt;
					cam.position.elements[2] -= Math.cos(yaw) * 20 * dt;
				}
				if (this.myKeys.keydown[83]) // back - s
				{
					cam.position.elements[0] += Math.sin(yaw) * 20 * dt;
					cam.position.elements[2] += Math.cos(yaw) * 20 * dt;
				}
				if (this.myKeys.keydown[65]) //left - a
				{
					cam.position.elements[0] -= Math.cos(yaw) * 20 * dt;
					cam.position.elements[2] += Math.sin(yaw) * 20 * dt;
				}
				if (this.myKeys.keydown[68]) //right - d
				{
					cam.position.elements[0] += Math.cos(yaw) * 20 * dt;
					cam.position.elements[2] -= Math.sin(yaw) * 20 * dt;
				}
				if (this.myKeys.keydown[81]) //down - q
				{
					cam.position.elements[1] -= 20 * dt;
				}
				if (this.myKeys.keydown[69]) //up - e
				{
					cam.position.elements[1] += 20 * dt; 
				}
				
				// Inverted up/down
				if (this.myKeys.keydown[38]) //up
				{
					cam.rotation.elements[0] -= 2 * dt; //look up
				}
				if (this.myKeys.keydown[40]) //down
				{
					cam.rotation.elements[0] += 2 * dt; //peer down
				}
				if (this.myKeys.keydown[37]) //left
				{
					cam.rotation.elements[1] += 2 * dt; //look left
				}
				if (this.myKeys.keydown[39]) //right
				{
					cam.rotation.elements[1] -= 2 * dt; //peer right
				}
				
				cam.rotation.elements[0] = clamp(cam.rotation.elements[0], -1.5, 1.5);
				
				let x = -Math.sin(cam.rotation.elements[1] - 0.4) * 1.5;
				let z = -Math.cos(cam.rotation.elements[1] - 0.4) * 1.5;
				
				let tx = -Math.sin(cam.rotation.elements[1] - 0.42) * 1.5;
				let tz = -Math.cos(cam.rotation.elements[1] - 0.42) * 1.5;
				
				this.torchParticle.transform.position.elements[0] = x + cam.position.elements[0];
				this.torchParticle.transform.position.elements[1] = cam.position.elements[1] - 0.3;
				this.torchParticle.transform.position.elements[2] = z + cam.position.elements[2];
				
				this.torch.transform.position.elements[0] = tx + cam.position.elements[0];
				this.torch.transform.position.elements[1] = cam.position.elements[1] - 0.5;
				this.torch.transform.position.elements[2] = tz + cam.position.elements[2];
				
				this.torchLight.position = this.torch.transform.position;
				
				this.torch.transform.rotation.elements[1] = cam.rotation.elements[1];

				if (this.myKeys.keydown[80]) {
					this.musicPaused = true;
					this.musicPlayer.pause();
				}
				if (this.myKeys.keydown[79]) {
					this.musicPaused = false;
					this.musicPlayer.resume();
				}
			},

			calculateDeltaTime: function(){
				var now,fps;
				now = performance.now(); 
				fps = 1000 / (now - this.lastTime);
				// fps = clamp(fps, 12, 60);
				this.lastTime = now; 
				return 1/fps;
			},

			doMousedown: function(e) {
				var mouse = getMouse(e);

				this.pmouse = mouse;
				this.canvas.requestPointerLock();

				this.resumeGame();
			},

			lockChangeAlert: function(e) {
				if (document.pointerLockElement === this.canvas || document.mozPointerLockElement === this.canvas) {
					this.resumeGame();
				} else {
					// this.pauseGame();
				}
			},

			pauseGame: function() {
				if(this.gameState === this.GAME_STATE.DEFAULT) {
					cancelAnimationFrame(this.animationID);
					this.gameState = this.GAME_STATE.PAUSED;
					this.handleKeyPress = false;
					this.update();
					this.musicPlayer.pause();
				}
			},

			resumeGame: function() {
				if(this.gameState === this.GAME_STATE.BEGIN || this.gameState === this.GAME_STATE.PAUSED) {
					cancelAnimationFrame(this.animationID);
					this.gameState = this.GAME_STATE.DEFAULT;
					this.handleKeyPress = true;
					this.update();
					if(!this.musicPaused) {
						this.musicPlayer.resume();
					}
				}
			},

			getAltitude: function(period, time, shift) {
				if (period == 0)
					return 0.25;
				var f1 = (time % period) / period - 0.25;

				if (f1 < 0.0)
					++f1;

				if (f1 > 1.0)
					--f1;

				var f2 = f1;
				f1 = 1.0 - (Math.cos(f1 * Math.PI) + 1.0) / 2.0;
				f1 = f2 + (f1 - f2) / 3.0;
				return f1 + shift / 360.0 + 0.25;
			},

			getSkyColor: function(altitude) {
				return Math.cos((altitude + 0.5) * Math.PI * 2.0) * 2.0 + 0.5;
			},

			getSkyBlend: function() {
				var temp = 2.0; //MC "Desert" biome
				var k = Math.max(-1.0, Math.min(1.0, temp / 3.0));
				return $V([0.6222 - k * 0.05, 0.5 + k * 0.1, 1.0]);
			},

			handleSky: function() {
				var sunAlt = this.getAltitude(this.cycleTime, this.worldTime, 0);
				var moonAlt = this.getAltitude(this.cycleTime, this.worldTime, 180);

				var gradient = Math.max(0, Math.min(1, this.getSkyColor(sunAlt)));
				var skyColor = this.getSkyBlend().multiply(gradient);

				this.graphics.skyColor().elements = skyColor.elements;
				this.graphics.setAmbient(skyColor);

				var camPos = this.graphics.getActiveCamera().transform.position;
				this.drawCelestial(this.sunRender, sunAlt, camPos);
				this.drawCelestial(this.moonRender, moonAlt, camPos);
				
				this.sun.intensity = $V([0.5, 0.4, 0.4]).multiply(gradient);
				
				this.worldTime++;
			},

			drawCelestial: function(obj, altitude, camPos) {
				var a = ((altitude + 0.5) * Math.PI * 2) % (Math.PI * 2);
				obj.transform.position = $V([0, 180, 0]).rotate(a, $L([0, 0, 0], [0, 0, 1])).add(camPos);
			},

			readableTime: function() {
				var ticks = ((this.worldTime / this.cycleTime) % 1.0) * 1440;
				var theHour = Math.floor(ticks / 60);
				var absHour = Math.abs(theHour);
				var tMinute = Math.floor(ticks % 60);
				var aMinute = Math.abs(tMinute);
				var aMin = (aMinute < 10 ? "0" : "") + aMinute;
				return ((theHour < 0 || tMinute < 0) ? "-" : "") + absHour + ":" + aMin;
			},

		}
	}()
);
