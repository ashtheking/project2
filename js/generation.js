/*jslint browser:true */
/*jslint plusplus: true */
/*
	World Generation Worker
	Copyright Ashwin Ganapathiraju, Kenneth Holland, 2016
	Contact for other usage at: axg3886@rit.edu or krh6591@rit.edu
*/

var GEN_STATE = Object.freeze({
	START: "Preparing Resources",
	WORLD: "Generating World",
	MESH:  "Building Meshes",
	END:   "Finishing up..."
});
var waitToGen = 20, genAmount = 0, genMax = 50, genState = undefined, world = {};

function returnObject(finished, meshData) {
	var genStr = "", genPercent = genAmount / genMax, k;
	for(k = 0; k < 1; k += 0.1) {
		genStr += (genPercent > k) ? "|" : " ";
	}
	genPercent = (genPercent * 100).toFixed(2);
	return {
		genMessage: genState,
		genStr: genStr,
		genPercent: genPercent,
		finished: finished,
		meshData: meshData
	};
}

function generateWorld() {
	if(genState === GEN_STATE.START) {
		if(waitToGen > 0) {
			waitToGen--;
			genAmount = genMax - waitToGen / 2;
		} else {
			//If we want to move resource loading here, perhaps?
			genState = GEN_STATE.WORLD;
			genAmount = 0;
			genMax = worldGen.CHUNK_SIZE + 1;
		}
		postMessage(returnObject(false));
	}
	else if(genState === GEN_STATE.WORLD) {
		if(genAmount == 0) {
			world = worldGen.makeWorld();
			world = worldGen.generateWorld(world);
			genAmount++;
		}
		else if(genAmount < genMax - 1) {
			for(var n = 0; n < worldGen.genTypes.length; n++) {
				var generator = worldGen.genTypes[n];
				for (var i = 0; i < worldGen.NUM_CHUNKS; i++) {
					for (var j = 0; j < worldGen.NUM_CHUNKS; j++) {
						generator.generate(world, i, j);
					}
				}
			}
			genAmount++;
		}
		else {
			genAmount = 0;
			genMax = world.length;
			genState = GEN_STATE.MESH;
		}
		postMessage(returnObject(false));
	}
	else if(genState === GEN_STATE.MESH) {
		if(genAmount < genMax) {
			var chunk = world.indexed(genAmount);
			var strings = [];
			for(var i = 0; i < 2; i++) {
				strings[i] = generateChunkMesh(chunk, i);
			}
			postMessage(returnObject(false, {str: strings, chunkIndex: genAmount, chunkX: chunk.globalX(0), chunkZ: chunk.globalZ(0)}));
			genAmount++;
		}

		if(genAmount >= genMax) {
			genState = GEN_STATE.END;
			postMessage(returnObject(true));
		}
	}
}

onmessage = function(event) {
	importScripts('utilities.js', 'gen/worldgen.js', 'gen/noise.js', 'gen/cavern.js',/* 'gen/dungeon.js',*/ 'gen/oregen.js', 'gen/trees.js');

	genState = GEN_STATE.START;
	while(genState != GEN_STATE.END) {
		generateWorld();
	}
};