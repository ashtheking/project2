/*jslint browser:true */
/*jslint plusplus: true */
/*
	Cavern Generator
	Copyright Ashwin Ganapathiraju, 2011-20worldGen.CHUNK_SIZE
	Exported to Javascript for: IGME-330 Project 2.
	Contact for other usage at: axg3886@rit.edu
*/
/* imported worldGen */
var worldGen = worldGen || {};

worldGen.genTypes.push(Object.freeze(
	function () {
		"use strict";

		function generateLargeCaveNode(xCenter, zCenter, world, x, y, z) {
			generateCaveNode(xCenter, zCenter, world, x, y, z, 1.0 + Math.random() * 6.0, 0.0, 0.0, -1, -1, 0.5);
		}

		function generateCaveNode(xCenter, zCenter, world, startX, startY, startZ, angleX, angleY, angleZ, var1, var2, var3) {
			var centerX = xCenter * worldGen.CHUNK_SIZE + 8;
			var centerZ = zCenter * worldGen.CHUNK_SIZE + 8;
			var angleVarY = 0.0;
			var angleVarZ = 0.0;

			if (var2 <= 0) {
				var chunkRange = 8 * worldGen.CHUNK_SIZE - worldGen.CHUNK_SIZE;
				var2 = chunkRange - nextInt(chunkRange / 4);
			}

			var breaker = false;

			if (var1 == -1) {
				var1 = var2 / 2;
				breaker = true;
			}

			var genMoreCaves = nextInt(var2 / 2) + var2 / 4;

			for (var randTurn = nextInt(6) == 0; var1 < var2; ++var1) {
				var modXY = 1.5 + Math.sin(var1 * Math.PI / var2) * angleX;
				var modXZ = modXY * var3;
				var cosZ = Math.cos(angleZ);
				var sinZ = Math.sin(angleZ);
				startX += Math.cos(angleY) * cosZ;
				startY += sinZ;
				startZ += Math.sin(angleY) * cosZ;

				if (randTurn) {
					angleZ *= 0.92;
				}
				else {
					angleZ *= 0.7;
				}

				angleZ += angleVarZ * 0.1;
				angleY += angleVarY * 0.1;
				angleVarZ *= 0.9;
				angleVarY *= 0.75;
				angleVarZ += (Math.random() - Math.random()) * Math.random() * 2.0;
				angleVarY += (Math.random() - Math.random()) * Math.random() * 4.0;

				if (!breaker && var1 == genMoreCaves && angleX > 1.0 && var2 > 0) {
					generateCaveNode(xCenter, zCenter, world, startX, startY, startZ, Math.random() * 0.5 + 0.5, angleY - (Math.PI / 2), angleZ / 3.0, var1, var2, 1.0);
					generateCaveNode(xCenter, zCenter, world, startX, startY, startZ, Math.random() * 0.5 + 0.5, angleY + (Math.PI / 2), angleZ / 3.0, var1, var2, 1.0);
					return;
				}

				if (!breaker && nextInt(4) == 0) {
					continue;
				}
				var displaceX = startX - centerX;
				var displaceZ = startZ - centerZ;
				var d10 = var2 - var1;
				var scale = angleX + 2.0 + worldGen.CHUNK_SIZE;

				if (displaceX * displaceX + displaceZ * displaceZ - d10 * d10 > scale * scale) { return; }

				if (startX >= centerX - worldGen.CHUNK_SIZE - modXY * 2.0 && 
					startZ >= centerZ - worldGen.CHUNK_SIZE - modXY * 2.0 && 
					startX <= centerX + worldGen.CHUNK_SIZE + modXY * 2.0 && 
					startZ <= centerZ + worldGen.CHUNK_SIZE + modXY * 2.0) {
					var tminX = Math.floor(startX - modXY) - xCenter * worldGen.CHUNK_SIZE - 1,
						tmaxX = Math.floor(startX + modXY) - xCenter * worldGen.CHUNK_SIZE + 1,
						tminY = Math.floor(startY - modXZ) - 1,
						tmaxY = Math.floor(startY + modXZ) + 1,
						tminZ = Math.floor(startZ - modXY) - zCenter * worldGen.CHUNK_SIZE - 1,
						tmaxZ = Math.floor(startZ + modXY) - zCenter * worldGen.CHUNK_SIZE + 1;

					if (tminX < 0) {
						tminX = 0;
					}

					if (tmaxX > worldGen.CHUNK_SIZE) {
						tmaxX = worldGen.CHUNK_SIZE;
					}

					if (tminY < 1) {
						tminY = 1;
					}

					if (tmaxY > worldGen.CHUNK_HEIGHT - 8) {
						tmaxY = worldGen.CHUNK_HEIGHT - 8;
					}

					if (tminZ < 0) {
						tminZ = 0;
					}

					if (tmaxZ > worldGen.CHUNK_SIZE) {
						tmaxZ = worldGen.CHUNK_SIZE;
					}

					var hitOcean = false;

					for (var localX = tminX; !hitOcean && localX < tmaxX; ++localX) {
						for (var localZ = tminZ; !hitOcean && localZ < tmaxZ; ++localZ) {
							for (var localY = tmaxY + 1; !hitOcean && localY >= tminY - 1; --localY) {
								var x = xCenter * worldGen.CHUNK_SIZE + localX,
									z = zCenter * worldGen.CHUNK_SIZE + localZ;

								if (worldGen.ifWorks(world, x, localY, z, false)) {
									if (world.get(x, localY, z) === worldGen.TYPES.water) {
										hitOcean = true;
									}

									if (localY != tminY - 1 && localX != tminX && localX != tmaxX - 1
											&& localZ != tminZ && localZ != tmaxZ - 1) {
										localY = tminY;
									}
								}
							}
						}
					}

					if (!hitOcean) {
						for (var localX = tminX; localX < tmaxX; ++localX) {
							var modX = (localX + xCenter * worldGen.CHUNK_SIZE + 0.5 - startX) / modXY;

							for (var localZ = tminZ; localZ < tmaxZ; ++localZ) {
								var modZ = (localZ + zCenter * worldGen.CHUNK_SIZE + 0.5 - startZ) / modXY;
								var localNum = (localX * worldGen.CHUNK_SIZE + localZ) * worldGen.CHUNK_HEIGHT;

								if (modX * modX + modZ * modZ < 1.0) {
									for (var localY = tmaxY - 1; localY >= tminY; --localY) {
										var realY = (localY + 0.5 - startY) / modXZ;
										var x = xCenter * worldGen.CHUNK_SIZE + localX,
											z = zCenter * worldGen.CHUNK_SIZE + localZ;

										if (realY > -0.7 && modX * modX + realY * realY + modZ * modZ < 1.0
												&& worldGen.ifWorks(world, x, localY, z, false)) {
											var block = world.get(x, localY, z);

											if (block === worldGen.TYPES.stone || block === worldGen.TYPES.dirt || block === worldGen.TYPES.grass
												|| block === worldGen.TYPES.iron || block === worldGen.TYPES.gold) {
												world.set(x, localY, z, (localY < 10) ? worldGen.TYPES.lava : worldGen.TYPES.air);
											}
										}
									}
								}
							}

							if (breaker) {
								break;
							}
						}
					}
				}
			}
		}

		function genCaves(world, xChunk, zChunk, xCenter, zCenter) {
			var numBigNodes = nextInt(nextInt(nextInt(30) + 1) + 5);
			var chunk = world.getChunk(xChunk, zChunk);

			if (nextInt(25) != 0) {
				numBigNodes = 0;
			}
			for (var i = 0; i < numBigNodes; ++i) {
				var cX = xChunk * worldGen.CHUNK_SIZE + nextInt(worldGen.CHUNK_SIZE);
				var cY = nextInt(worldGen.CHUNK_HEIGHT - 8) + 8;
				var cZ = zChunk * worldGen.CHUNK_SIZE + nextInt(worldGen.CHUNK_SIZE);
				var numSmallNodes = 1;

				if (nextInt(4) == 0) {
					generateLargeCaveNode(xCenter, zCenter, world, cX, cY, cZ);
					numSmallNodes += nextInt(4);
				}

				for (var j = 0; j < numSmallNodes; ++j) {
					var angleY = Math.random() *  Math.PI * 2.0;
					var angleZ = (Math.random() - 0.5) * 0.25;
					var angleX = Math.random() * 2.0 + Math.random();

					if (nextInt(10) == 0) {
						angleX *= Math.random() * Math.random() * 3.0 + 1.0;
					}

					generateCaveNode(xCenter, zCenter, world, cX, cY, cZ, angleX, angleY, angleZ, 0, 0, 1.0);
				}
			}
		}

		function generate(world, i, j) {
			for (var x = i - 8; x <= i + 8; x++) {
				for (var y = j - 8; y <= j + 8; y++) {
					genCaves(world, i, j, x, y);
				}
			}
			return world;
		}

		return {
			generate: generate
		};
	}()
));