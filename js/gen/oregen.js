/*jslint browser:true */
/*jslint plusplus: true */
/*
	Ore Generator
	Copyright Ashwin Ganapathiraju, 2014-2016
	Exported to Javascript for: IGME-330 Project 2.
	Contact for other usage at: axg3886@rit.edu
*/
/* imported worldGen */
var worldGen = worldGen || {};

worldGen.genTypes.push(Object.freeze(
	function () {
		"use strict";

		var ironOre = {maxVeins: 4.0, numBlocks: 24, maxY: 128, block: worldGen.TYPES.iron},
			goldOre = {maxVeins: 0.4, numBlocks: 24, maxY:  32, block: worldGen.TYPES.gold};

		function genVein(world, x, y, z, numBlocks, block) {
			var angle = Math.random() * Math.PI,
				xMin = x + 8 + Math.sin(angle) * numBlocks / 8.0,
				xMax = x + 8 - Math.sin(angle) * numBlocks / 8.0,
				zMin = z + 8 + Math.cos(angle) * numBlocks / 8.0,
				zMax = z + 8 - Math.cos(angle) * numBlocks / 8.0,
				yMin = y + nextInt(3) - 2,
				yMax = y + nextInt(3) - 2,
				iter, xPos, yPos, zPos, mod, xBot, yBot, zBot, xTop, yTop, zTop, i, xArc, j, yArc, k, zArc, b;

			for (iter = 0; iter <= numBlocks; ++iter) {
				xPos = xMin + (xMax - xMin) * iter / numBlocks;
				yPos = yMin + (yMax - yMin) * iter / numBlocks;
				zPos = zMin + (zMax - zMin) * iter / numBlocks;
				mod = (Math.sin(iter * Math.PI / numBlocks) + 1.0) * (Math.random() * numBlocks / 16.0) + 1.0;
				xBot = Math.floor(xPos - mod / 2.0);
				xTop = Math.floor(xPos + mod / 2.0);
				yBot = Math.floor(yPos - mod / 2.0);
				yTop = Math.floor(yPos + mod / 2.0);
				zBot = Math.floor(zPos - mod / 2.0);
				zTop = Math.floor(zPos + mod / 2.0);

				for (i = xBot; i <= xTop; ++i) {
					xArc = (i + 0.5 - xPos) / (mod / 2.0);

					if (xArc * xArc < 1.0)
						for (j = yBot; j <= yTop; ++j) {
							yArc = (j + 0.5 - yPos) / (mod / 2.0);

							if (xArc * xArc + yArc * yArc < 1.0)
								for (k = zBot; k <= zTop; ++k) {
									zArc = (k + 0.5 - zPos) / (mod / 2.0);

									if(worldGen.ifWorks(world, i, j, k, false)) {
										b = world.get(i, j, k);
										if (xArc * xArc + yArc * yArc + zArc * zArc < 1.0)
											if (b === worldGen.TYPES.stone) {
												world.set(i, j, k, block);
											}
									}
								}
						}
				}
			}
		}
		
		function genOre(world, i, j, ore) {
			var veins, x, y, z;
			veins = ore.maxVeins;
			while(veins >= 1.0) {
				x = i * worldGen.CHUNK_SIZE + nextInt(worldGen.CHUNK_SIZE);
				y = nextInt(ore.maxY);
				z = j * worldGen.CHUNK_SIZE + nextInt(worldGen.CHUNK_SIZE);
				genVein(world, x, y, z, ore.numBlocks, ore.block);
				veins--;
			}
			if(Math.random() < veins) {
				x = i * worldGen.CHUNK_SIZE + nextInt(worldGen.CHUNK_SIZE);
				y = nextInt(ore.maxY);
				z = j * worldGen.CHUNK_SIZE + nextInt(worldGen.CHUNK_SIZE);
				genVein(world, x, y, z, ore.numBlocks, ore.block);
			}
		}


		function generate(world, i, j) {
			genOre(world, i, j, ironOre);
			genOre(world, i, j, goldOre);
			return world;
		}
		
		return {
			generate: generate
		};
	}()
));