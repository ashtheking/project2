/*jslint browser:true */
/*jslint plusplus: true */ /* JSLint is nice, but it's ridiculous that '++' is "confusing" */
/* exported worldGen */
/*
	World Generator API
	Copyright Ashwin Ganapathiraju, 2011-2016
	Exported to Javascript for: IGME-330 Project 2.
	Contact for other usage at: axg3886@rit.edu
*/

var worldGen = worldGen || Object.freeze(
	function () {
		"use strict";

		/** Type Data **/

		var TYPES = Object.freeze({
			air: 0,
			stone: 1,
			dirt: 2,
			grass: 3,
			wall: 4,
			iron: 5,
			gold: 6,
			water: 7,
			lava: 8,
			stair: 9,
			log: 10,
			leaf: 11,
			sand: 12,
		});

		var TYPE_CUBE = makeTypeArray(true);
		TYPE_CUBE[TYPES.air] = false;
		TYPE_CUBE[TYPES.stair] = false;

		var TYPE_OPAQUE = makeTypeArray(true);
		TYPE_OPAQUE[TYPES.water] = false;
		TYPE_OPAQUE[TYPES.lava] = false;
		TYPE_OPAQUE[TYPES.leaf] = false;
		
		var TYPE_TEXTURES = makeTypeArray("");
		TYPE_TEXTURES[TYPES.stair] = "wood";

		function makeTypeArray(init) {
			var a = [];
			for (var key in TYPES) {
				if (TYPES.hasOwnProperty(key)) {
					a[TYPES[key]] = init;
				}
			}
			return a;
		}

		/** Public Accessor Data **/

		var genTypes = [];
		
		var CHUNK_SIZE = 16;
		var CHUNK_HEIGHT = 128;
		var NUM_CHUNKS = 8;
		var WORLD_SIZE = NUM_CHUNKS * CHUNK_SIZE;
		var SEA_LEVEL = 50;

		/** Methods **/

		function makeArr() {
			var arr = [],
				N = CHUNK_SIZE, //Math.pow(2, p),
				get = function (x, z) { return arr[x * N + z]; },
				set = function (x, z, v) { arr[x * N + z] = v; },
				x,
				z;

			for (x = 0; x < N; x++) {
				for (z = 0; z < N; z++) {
					set(x, z, TYPES.stone);
				}
			}

			return Object.seal({
				get: get,
				set: set,
				stairs: undefined
			});
		}
		
		function makeChunk(i,j) {
			var chunk = [],
				heightMap = [],
				get = function(x, y, z) { return chunk[y].get(x, z); },
				set = function(x, y, z, v) { chunk[y].set(x, z, v); },
				globalX = function(x) { return (i << 4) + x; },
				globalZ = function(z) { return (j << 4) + z; },
				height = function(x, z) { return heightMap[x * CHUNK_SIZE + z]; },
				k, x, z;

			for(k = 0; k < CHUNK_HEIGHT; k++) {
				chunk[k] = makeArr();
			}
			
			for (x = 0; x < CHUNK_SIZE; x++) {
				for (z = 0; z < CHUNK_SIZE; z++) {
					heightMap[x * CHUNK_SIZE + z] = Math.floor((perlinNoise.octave(globalX(x) / (WORLD_SIZE), globalZ(z) / (WORLD_SIZE), 0, 3, 1.1) + 0.6) * CHUNK_HEIGHT * 2 / 3);
				}
			}
			
			return Object.freeze({
				get: get,
				set: set,
				randomElement: chunk.randomElement.bind(chunk),
				globalX: globalX,
				globalZ: globalZ,
				chunkX: i,
				chunkZ: j,
				height: height
			});
		}

		function makeWorld() {
			var world = [],
				indexed = function(i) { return world[i]; },
				getChunk = function(i, j) { return indexed(i * NUM_CHUNKS + j); },
				setChunk = function(i, j, v) { world[i * NUM_CHUNKS + j] = v; },
				chunk = function(x, z) { return getChunk(x >> 4, z >> 4); },
				get = function(x, y, z) { return chunk(x, z).get(x % 16, y, z % 16); },
				set = function(x, y, z, v) { chunk(x, z).set(x % 16, y, z % 16, v); },
				height = function(x, z) { return chunk(x, z).height(x % 16, z % 16); },
				i,
				j;

			perlinNoise.seed(Math.random() * 25);

			for(i = 0; i < NUM_CHUNKS; i++) {
				for(j = 0; j < NUM_CHUNKS; j++) {
					setChunk(i, j, makeChunk(i, j));
				}
			}

			return Object.freeze({
				get: get,
				set: set,
				getChunk: getChunk,
				setChunk: setChunk,
				height: height,
				randomElement: world.randomElement.bind(chunk),
				length: world.length,
				indexed: indexed,
			});
		}

		function nextTo(world, x, y, z, v) {
			if (x - 1 >= 0) {
				if (world.get(x - 1, y, z) === v) { //left
					return 1;
				}
			}
			if (z - 1 >= 0) {
				if (world.get(x, y, z - 1) === v) { //top
					return 2;
				}
			}
			if (x + 1 < WORLD_SIZE) {
				if (world.get(x + 1, y, z) === v) { //right
					return 3;
				}
			}
			if (z + 1 < WORLD_SIZE) {
				if (world.get(x, y, z + 1) === v) { //bottom
					return 4;
				}
			}
			if (x - 1 >= 0 && z - 1 >= 0) {
				if (world.get(x - 1, y, z - 1) === v) {
					return 5;
				}
			}
			if (x - 1 >= 0 && z + 1 < WORLD_SIZE) {
				if (world.get(x - 1, y, z + 1) === v) {
					return 6;
				}
			}
			if (x + 1 < WORLD_SIZE && z - 1 >= 0) {
				if (world.get(x + 1, z, z - 1) === v) {
					return 7;
				}
			}
			if (x + 1 < WORLD_SIZE && z + 1 < WORLD_SIZE) {
				if (world.get(x + 1, y, z + 1) === v) {
					return 8;
				}
			}
			return 0;
		}

		function ifWorks(world, x, y, z, empty) {
			return !(x < 0 || x >= WORLD_SIZE || y < 0 || y >= CHUNK_HEIGHT || z < 0 || z >= WORLD_SIZE)
			 && (empty ? world.get(x, y, z) === TYPES.stone : true);
		}

		function countNext(world, x, y, z, c) {
			var k = 0;
			if (x - 1 >= 0) {
				if (world.get(x - 1, y, z) === c) {
					k++;
				}
			}
			if (z - 1 >= 0) {
				if (world.get(x, y, z - 1) === c) {
					k++;
				}
			}
			if (x + 1 < CHUNK_SIZE) {
				if (world.get(x + 1, y, z) === c) {
					k++;
				}
			}
			if (z + 1 < CHUNK_SIZE) {
				if (world.get(x, y, z + 1) === c) {
					k++;
				}
			}
			return k;
		}

		function fillRect(world, x, y, z, w, h, t) {
			var m = w * h, k = 0, i, j;
			for (i = 0; i < w; i++) {
				for (j = 0; j < h; j++) {
					if (ifWorks(world, x + i, y, z + j, true)) {
						k++;
					}
				}
			}
			if (k < 3 * m / 4) {
				return false;
			}
			for (i = 0; i < w; i++) {
				for (j = 0; j < h; j++) {
					if (ifWorks(world, x + i, y, z + j, false)) {
						world.set(x + i, y, z + j, t);
					}
					if (ifWorks(world, x + i, y - 1, z + j, false)) {
						world.set(x + i, y - 1, z + j, t);
					}
				}
			}
			return true;
		}

		function genWalls(world) {
			var i, j, chunk, x, y, z;

			for(i = 0; i < NUM_CHUNKS; i++) {
				for(j = 0; j < NUM_CHUNKS; j++) {
					chunk = world.getChunk(i, j);
					for(y = 0; y < CHUNK_HEIGHT; y++) {
						for (x = 0; x < CHUNK_SIZE; x++) {
							for (z = 0; z < CHUNK_SIZE; z++) {
								if ((nextTo(chunk, x, y, z, TYPES.air) !== 0 || nextTo(chunk, x, y, z, TYPES.stair) !== 0) && 
									(chunk.get(x, y, z) === TYPES.stone || chunk.get(x, y, z) === TYPES.dirt || chunk.get(x, y, z) === TYPES.grass)) {
									chunk.set(x, y, z, TYPES.wall);
								}
							}
						}
					}
				}
			}
		}

		function getRandomWall(world, y, k) {
			var x = nextInt(WORLD_SIZE),
				z = nextInt(WORLD_SIZE),
				q = nextTo(world, x, y, z, TYPES.air);
			return world.get(x, y, z) === TYPES.wall && (q > 0 && q < 5) ? {x: x, y: y, z: z} : k < 200 ? getRandomWall(world, y, k++) : undefined;
		}

		function generateWorld(world) {
			var i, j, chunk, k, x, y, z, h, t;

			for(i = 0; i < NUM_CHUNKS; i++) {
				for(j = 0; j < NUM_CHUNKS; j++) {
					chunk = world.getChunk(i, j);

					for(y = 0; y < CHUNK_HEIGHT; y++) {
						for(x = 0; x < CHUNK_SIZE; x++) {
							for(z = 0; z < CHUNK_SIZE; z++) {
								h = chunk.height(x, z);
								t = chunk.get(x, y, z);
								if(h < y) {
									chunk.set(x, y, z, (y < SEA_LEVEL) ? TYPES.water : TYPES.air);
								}
								if(h == y && t === TYPES.stone) {
									chunk.set(x, y, z, (y <= SEA_LEVEL) ? TYPES.sand : TYPES.grass);
								}
								if(h == y + 1 && t === TYPES.stone) {
									chunk.set(x, y, z, TYPES.dirt);
								}
							}
						}
					}
				}
			}
			return world;
		}

		return Object.freeze({
			TYPES: TYPES,
			TYPE_CUBE: TYPE_CUBE,
			TYPE_OPAQUE: TYPE_OPAQUE,
			TYPE_TEXTURES: TYPE_TEXTURES,
			genTypes: genTypes,
			CHUNK_HEIGHT: CHUNK_HEIGHT,
			CHUNK_SIZE: CHUNK_SIZE,
			NUM_CHUNKS: NUM_CHUNKS,
			WORLD_SIZE: WORLD_SIZE,
			nextTo: nextTo,
			ifWorks: ifWorks,
			countNext: countNext,
			fillRect: fillRect,
			genWalls: genWalls,
			getRandomWall: getRandomWall,
			makeWorld: makeWorld,
			generateWorld: generateWorld
		});
	}()
);