/*jslint browser:true */
/*jslint plusplus: true */
/*
	Tree Generator
	Copyright Ashwin Ganapathiraju, 2014-2016
	Exported to Javascript for: IGME-330 Project 2.
	Contact for other usage at: axg3886@rit.edu
*/
/* imported worldGen */
var worldGen = worldGen || {};

worldGen.genTypes.push(Object.freeze(
	function () {
		"use strict";

		function genTree(world, x, y, z) {
			var height = nextInt(8) + 4;
			var upper = y + height + 1;

			if(y >= 1 && upper < worldGen.CHUNK_HEIGHT) {
				for (var j = 0; j < height; ++j) {
					if(!worldGen.ifWorks(world, x, y + j, z, false) || world.get(x, y + j, z) !== worldGen.TYPES.air) {
						return;
					}
				}

				var below = world.get(x, y-1, z);
				
				if((below === worldGen.TYPES.dirt || below === worldGen.TYPES.grass) && y < worldGen.CHUNK_HEIGHT - height - 1) {
					world.set(x, y - 1, z, worldGen.TYPES.dirt);
	
					for(var j = y - 3 + height; j < upper; ++j) {
						var n = j - (y + height);
						var width = 1 - Math.floor(n / 2);
						for(var i = x - width; i <= x + width; ++i) {
							for(var k = z - width; k <= z + width; ++k) {
								if(worldGen.ifWorks(world, i, j, k, false)) {
									if(Math.abs(i - x) != width || Math.abs(k - z) != width) {
										if(world.get(i, j, k) === worldGen.TYPES.air) {
											world.set(i, j, k, worldGen.TYPES.leaf);
										}
									}
								}
							}
						}
					}
					for (var j = 0; j < height; ++j) {
						world.set(x, y + j, z, worldGen.TYPES.log);
                    }
				}
			}
		}

		function generate(world, i, j) {
			for(var k = 0; k < 20; k++) {
				var x = i * worldGen.CHUNK_SIZE + nextInt(worldGen.CHUNK_SIZE);
				var z = j * worldGen.CHUNK_SIZE + nextInt(worldGen.CHUNK_SIZE);
				var y = world.height(x, z);
				genTree(world, x, y, z);
			}
			return world;
		}
		
		return {
			generate: generate
		};
	}()
));